using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
// Bibliothèque pour charger les scènes
using UnityEngine.SceneManagement;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public static event Action OnGameStarted;
    static public event Action OnGameEnded;

    public enum GameState
    {
        MainMenu,
        InGame,
        GameOver
    }

    public GameState CurrentState;

    void Awake()
    {
        Instance = this;

        // Mettre les fps à 60 car android et apple limite de base à 30 fps
        Application.targetFrameRate = 60;
    }

    public void StartGame()
    {
        CurrentState = GameState.InGame;
        OnGameStarted?.Invoke();
    }

    public void GameOver()
    {
        CurrentState = GameState.GameOver;
        OnGameEnded?.Invoke();
    }

    public void RestartGame()
    {
        SceneManager.LoadSceneAsync(0);
    }
}
