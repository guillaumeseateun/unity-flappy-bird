using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    public float backgroundSpeed = 0.1f;
    float spriteSize;

    // Récuperer info background
    public SpriteRenderer backgroundSprite;

    // stocker la startposition
    Vector3 startPosition;

    void Start()
    {
        // Récupération de la première position du background
        startPosition = transform.position;
        // Récupération de la taille du sprite du background sur l'axe x 
        spriteSize = backgroundSprite.sprite.bounds.size.x * backgroundSprite.transform.localScale.x;
    }

    void Update()
    {
        // Renvoie le reste de la division du premier argument de la fonction par le second
        float newPosition = Mathf.Repeat(Time.time * backgroundSpeed, spriteSize);
        transform.position = startPosition + Vector3.left * newPosition;
    }
}
